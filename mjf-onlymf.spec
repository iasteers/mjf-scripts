Name: mjf-onlymf
Version: %(echo ${MJF_VERSION:-0.0})
Release: 1
BuildArch: noarch
Summary: Machine Features (no Job Features) 
License: BSD
Group: System Environment/Daemons
Source: mjf-scripts.tgz
Vendor: GridPP
Packager: Andrew McNab <Andrew.McNab@cern.ch>

%description
Machine Features implementation following HSF-TN-2016-02
(install mjf-torque or mjf-htcondor instead to get both 
 Machine and Job Features)

%prep

%setup -n mjf-scripts

%build

%install
make onlymf-install

%post
chkconfig mjf on
service mjf start

%preun
if [ "$1" = "0" ] ; then
  # if uninstallation rather than upgrade then stop
  chkconfig mjf off
  service mjf stop
fi

%files
/etc/rc.d/init.d/*
/etc/profile.d/*
